Changelog
=========


(unreleased)
------------

Fix
~~~
- Add __init__.py. [Stavros Korokithakis]
- Rename `terse` argument. [Stavros Korokithakis]

Other
~~~~~
- Feat: Add weii man page. [Stavros Korokithakis]
- Feat: Add cli script in the pyproject config. [Stavros Korokithakis]
- Feat: Rename to `weii` [Stavros Korokithakis]
- Build: Release v0.1.1. [Stavros Korokithakis]
- Doc: Documentation updates. [Stavros Korokithakis]
- Switch to ruff. [Stavros Korokithakis]
- Chore: Add repository and homepage. [Stavros Korokithakis]
- Feat: Abort if the user presses the board button while measuring.
  [Stavros Korokithakis]
- Feat: Add the `--command` argument. [Stavros Korokithakis]
- Add LICENSE. [Stavros Korokithakis]
- Doc: Add changelog. [Stavros Korokithakis]
- Initial commit. [Stavros Korokithakis]


